package com.example.whoareyou;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.whoareyou.models.FirebaseDoorbell;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HistoryActivity extends AppCompatActivity {
    public HistoryListAdapter adapter;
    private ArrayList<FirebaseDoorbell> listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Button button = (Button) findViewById(R.id.BackButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        listItems = new ArrayList<FirebaseDoorbell>();
        final ListView listView = (ListView) findViewById(R.id.listview);

        adapter = new HistoryListAdapter(this, listItems);

        listView.setAdapter(adapter);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("doorbell").child(Constants.RASP_ID);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listItems.clear();
                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    FirebaseDoorbell doorbell = singleSnapshot.getValue(FirebaseDoorbell.class);

                    listItems.add(doorbell);
                }
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("fail :/", "onCancelled", databaseError.toException());
            }
        });
    }
}

