package com.example.whoareyou.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;


@IgnoreExtraProperties
public class FirebaseDoorStatus {
    public String status;

    public FirebaseDoorStatus() {
        // Default constructor required for calls to DataSnapshot.getValue()
    }


    public FirebaseDoorStatus(String status) {
        this.status = status;

    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("status", status);


        return result;
    }
}
