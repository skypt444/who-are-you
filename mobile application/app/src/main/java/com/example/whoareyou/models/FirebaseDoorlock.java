package com.example.whoareyou.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;


@IgnoreExtraProperties
public class FirebaseDoorlock {
    public long time;
    public String function;

    public FirebaseDoorlock() {
        // Default constructor required for calls to DataSnapshot.getValue(FirebaseDoorlock.class)
    }

    public FirebaseDoorlock(long time, String function) {
        this.time = time;
        this.function = function;

    }

    public long getTime() {
        return time;
    }

    public String getFunction() {
        return function;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("time", time);
        result.put("function", function);

        return result;
    }
}