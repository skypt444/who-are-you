package com.example.whoareyou;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.whoareyou.models.FirebaseDoorbell;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class HistoryListAdapter extends BaseAdapter {

    Context context;
    private final ArrayList<FirebaseDoorbell> doorbells;

    public HistoryListAdapter(Context context, ArrayList<FirebaseDoorbell> doorbells) {
        this.context = context;
        this.doorbells = doorbells;
    }

    @Override
    public int getCount() {
        return doorbells.size();
    }

    @Override
    public Object getItem(int i) {
        return doorbells.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("StaticFieldLeak")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        final ViewHolder viewHolder;



        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.history_listview, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.description = (TextView) convertView.findViewById(R.id.l_description);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.l_image);



            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }

        FirebaseDoorbell doorbell = doorbells.get(position);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Date date=new Date(doorbell.time);

        viewHolder.description.setText(String.format("Doorbell at %s", dateFormat.format(date)));

        new AsyncTask<String, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... urls) {
                String urldisplay = urls[0];
                Bitmap mIcon11 = null;
                try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", Objects.requireNonNull(e.getMessage()));
                    e.printStackTrace();
                }
                return mIcon11;
            }

            protected void onPostExecute(Bitmap result) {
                viewHolder.image.setImageBitmap(result);
            }
        }.execute(doorbell.image_url);


        return convertView;
    }

    private static class ViewHolder {

        TextView description;
        ImageView image;

    }
}