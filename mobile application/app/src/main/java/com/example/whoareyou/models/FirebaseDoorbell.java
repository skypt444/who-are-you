package com.example.whoareyou.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;


@IgnoreExtraProperties
public class FirebaseDoorbell {
    public  long time;
    public  String image_url;

    public FirebaseDoorbell(){
    }

    public FirebaseDoorbell(long time, String image_url) {
        this.time = time;
        this.image_url = image_url;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("time", time);
        result.put("image_url", image_url);

        return result;
    }
}
