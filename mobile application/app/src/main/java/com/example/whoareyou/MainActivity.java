package com.example.whoareyou;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.whoareyou.models.FirebaseDoorStatus;
import com.example.whoareyou.models.FirebaseDoorlock;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

/*

//http://blog.naver.com/cosmosjs/220718005019 about webview
//https://firebase.google.com/ about firebase
//https://recipes4dev.tistory.com/150 about thread
//https://jeongchul.tistory.com/542 about audio
//https://m.blog.naver.com/PostView.nhn?blogId=cosmosjs&logNo=220666294999&proxyReferer=https%3A%2F%2Fwww.google.com%2F 외부 원격
//https://firebase.googleblog.com/2016/08/sending-notifications-between-android.html 노티피케이션!

 */
public class MainActivity extends AppCompatActivity {
    //global variable for door's open/close state and cameraView
    private boolean _speakerState;
    private boolean _speaking;

    private AudioManager audio;


    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent intent = new Intent(this, LoadingActivity.class);
        startActivity(intent);

        //fix the screen
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //basic state of global variables - door steate
        _speakerState = true;
        _speaking = false;
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        // variables to control this app
        Button openButton = (Button) findViewById(R.id.OpenButton);
        Button closeButton = (Button) findViewById(R.id.CloseButton);
        ImageButton histbutton = (ImageButton) findViewById(R.id.HistButton);

        final ImageButton speakerbutton = (ImageButton) findViewById(R.id.SpeakerButton);
        final ImageButton micbutton = (ImageButton) findViewById(R.id.MicButton);

        //variables to control the web view
        ImageButton videobutton = (ImageButton) findViewById(R.id.VideoButton);
        ImageButton pausebutton = (ImageButton) findViewById(R.id.PauseButton);
        webView = (WebView) findViewById(R.id.webview);
        webView.setPadding(0, 0, 0, 0);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setInitialScale(160);


        //this finction deals with OpenButton
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call AlertDialog message to make sure you really wanna open the door
                showMessage();
            }
        });


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "DOOR CLOSED", Toast.LENGTH_LONG).show();

                FirebaseDoorlock controlLock = new FirebaseDoorlock(System.currentTimeMillis(), "close");
                FirebaseDatabase.getInstance().getReference()
                        .child("doorlock").child(Constants.RASP_ID).push().setValue(controlLock);
            }
        });

        histbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
                startActivity(intent);
            }
        });

        speakerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Speaker pressed", Toast.LENGTH_LONG).show();

                if (_speakerState == true) //sound changed to mute
                {
                    speakerbutton.setSelected(true); // change to clicked image
                    setVolumeControlStream(AudioManager.STREAM_MUSIC);
                    audio.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_SHOW_UI); // process to make voulume zero
                    _speakerState = false; //
                } else //button click to chage volume to normal
                {
                    speakerbutton.setSelected(false);
                    setVolumeControlStream(AudioManager.STREAM_MUSIC);
                    audio.setStreamVolume(AudioManager.STREAM_MUSIC, 1, AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND); // process to make voulume zero
                    _speakerState = true; //다음에 누르면 색이 변하도록 값을 변경

                }

            }
        });


        //how can i control the image of view..?
        micbutton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (!_speaking) {
                            Toast.makeText(getApplicationContext(), "SPEAK!!!", Toast.LENGTH_LONG).show();
                            _speaking = true;
                        }

                    case MotionEvent.ACTION_UP:
                        _speaking = false;
                }
                return true;
            }
        });


        //default image of webview
        String url = "https://miro.medium.com/max/700/1*WwN78iOX-HC74T9AHGxjOg.png";
        webView.loadUrl(url);

        pausebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://cdn.hswstatic.com/gif/brakes-stopped-working-1.jpg";
                webView.loadUrl(url);
            }
        });
        //it's about webview play button
        videobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://192.168.43.197:8080";
                webView.loadUrl(url);
            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic(Constants.DOORBELL_NOTIF_NAME)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("subscribtion", "subscribed to the doorbell notif successfully");
                    }
                });

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("door_status").child(Constants.RASP_ID);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseDoorStatus doorStatus = dataSnapshot.getValue(FirebaseDoorStatus.class);
                Log.i("door_status", "door status changed " + doorStatus.status);
                SetDoorStatus(doorStatus.status.equals("open"));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    //This is AlertDialog message for the door open control
    private void showMessage() {
        //create AlertDialog message
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Wait!");
        builder.setMessage("Do you really want to open the door?!");
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //to get current time
                long now = System.currentTimeMillis();

                //push the function to open the door
                FirebaseDoorlock controlLock = new FirebaseDoorlock(now, "open");
                FirebaseDatabase.getInstance().getReference()
                        .child("doorlock").child(Constants.RASP_ID).push().setValue(controlLock);

                Toast.makeText(getApplicationContext(), "DOOR OPENED", Toast.LENGTH_LONG).show();

            }
        });
        builder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Toast.makeText(getApplicationContext(), "CANCELED", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Toast.makeText(getApplicationContext(), "DOOR CLOSED", Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void SetDoorStatus(boolean open) {
        ImageView doorstate = (ImageView) findViewById(R.id.DoorState);
        Resources res = getResources();

        BitmapDrawable bitmap = null;
        if (!open)
            bitmap = (BitmapDrawable) res.getDrawable(R.drawable.door_closed, null);
        else
            bitmap = (BitmapDrawable) res.getDrawable(R.drawable.door_opened, null);
        int bitmapWidth = bitmap.getIntrinsicWidth();
        int bitmapHeight = bitmap.getIntrinsicHeight();
        doorstate.setImageDrawable(bitmap);
        doorstate.getLayoutParams().width = bitmapWidth;
        doorstate.getLayoutParams().height = bitmapHeight;
    }
}
