#include <WiFi.h>
#include <PubSubClient.h>

#include <Adafruit_NeoPixel.h>


namespace wifi {
  constexpr char* SSID = "hoons";
  constexpr char* PASSWORD = "12341234";
}

namespace pins {
  constexpr int LED = 13;
  constexpr int BUTTON = 34;
}

namespace mqtt {
  constexpr char* SERVER = "192.168.1.94";
  constexpr int PORT = 1883;
  namespace topic {
    constexpr char* LED_STATUS = "DB_ledstatus";
    constexpr char* DOORBELL = "DB_doorbell";
  }
}

WiFiClient wifi_client;
PubSubClient mqtt_client(wifi_client);

char msg[50];
int value = 0;
bool doorbell_triggered = false;

void setup_wifi() {
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi::SSID);

  WiFi.begin(wifi::SSID, wifi::PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(wifi_client.localIP());
}

void callback_receive(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (strcmp(topic, mqtt::topic::LED_STATUS) == 0) {
    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == '1') {
      digitalWrite(pins::LED, HIGH);
      Serial.println("Switch on the led");
    } else {
      digitalWrite(pins::LED, LOW);
      Serial.println("Switch off the led");
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!mqtt_client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (mqtt_client.connect(clientId.c_str())) {
      Serial.println("connected");
      mqtt_client.subscribe(mqtt::topic::LED_STATUS);
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqtt_client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void check_button() {
  int button_state = digitalRead(pins::BUTTON);
  if (button_state == LOW && !doorbell_triggered)
  {
    Serial.println("Doorbell triggered");
    mqtt_client.publish(mqtt::topic::DOORBELL, "triggered");
    doorbell_triggered = true;
  }
  if (button_state == HIGH)
    doorbell_triggered = false;
}

void setup() {
  pinMode(pins::LED, OUTPUT);
  pinMode(pins::BUTTON, INPUT);
  Serial.begin(115200);

  delay(10);
  setup_wifi();

  mqtt_client.setServer(mqtt::SERVER, mqtt::PORT);
  mqtt_client.setCallback(callback_receive);
}

void loop() {
  if (!mqtt_client.connected()) {
    reconnect();
  }

  check_button();

  mqtt_client.loop();
}
