import sys
import base64
import io
import time

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

import requests

import paho.mqtt.client as mqtt
from http.server import BaseHTTPRequestHandler, HTTPServer

from face import Face
from streamer import CamHandler
from concurrent_camera import ConcurrentCamera

RASP_ID = '5432'

TOPIC_DOOR_BELL = "DB_doorbell"
TOPIC_DOOR_LED = "DB_ledstatus"
TOPIC_DOOR_STATUS = "DL_status"
TOPIC_DOOR_RELAY = "DL_relay"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe(TOPIC_DOOR_BELL)
    client.subscribe(TOPIC_DOOR_STATUS)


def on_message(client, userdata, msg):
    if msg.topic == TOPIC_DOOR_BELL:
        my_stream = camera.get_last_image()
        img_data = my_stream.getvalue()

        requests.post('https://us-central1-whoareyou-c7641.cloudfunctions.net/triggerDoorbell', json={
            "rasp_id": RASP_ID,
            "image_as_b64": base64.b64encode(img_data).decode(),
        })
    elif msg.topic == TOPIC_DOOR_STATUS:
        payload = msg.payload.decode()
        if payload == "0":
            door_status = "open"
        else:
            door_status = "close"
        requests.post('https://us-central1-whoareyou-c7641.cloudfunctions.net/setDoorStatus', json={
            "rasp_id": RASP_ID,
            "status": door_status,
        })

def sendIAmOnline():
    requests.post('https://us-central1-whoareyou-c7641.cloudfunctions.net/raspIsOnline', json={
        "rasp_id": RASP_ID,
        "address": '',
    })


def set_door_relay(should_be_open):
    if should_be_open:
        opcode = '1'
    else:
        opcode = '0'
    client.publish(TOPIC_DOOR_RELAY, opcode)


def door_open_listener(event):
    if event.event_type == 'put':
        if event.data is None or 'function' not in event.data:
            return
        function = event.data['function']
        set_door_relay(function == 'open')


def on_face_recognized(name):
    print('opening door for {}'.format(name))
    set_door_relay(True)
    time.sleep(2)
    set_door_relay(False)


print('initlializing MQTT...')
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("localhost", 1883, 60)
print('MQTT initlialized')

# change this one day :/
print('initlializing Firebase...')
cred = credentials.Certificate('serviceAccountKey.json')
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://whoareyou-c7641.firebaseio.com/'
})
db.reference('/doorlock/{}'.format(RASP_ID)).listen(door_open_listener)
print('Firebase initlialized')

print('initlializing Camera...')
camera = ConcurrentCamera()
print('Camera initlialized')

print('initlializing FaceRecognition...')
face = Face(camera, on_face_recognized)
print('FaceRecognition initlialized')

print('initlializing HttpServer...')
CamHandler.camera = camera
server = HTTPServer(('', 8080), CamHandler)
print('initlializing HttpServer...')

try:
    print('starting Camera...')
    camera.loop_start()
    print('starting MQTT...')
    client.loop_start()
    print('starting Face...')
    face.loop_start()
    print('starting Http...')
    server.serve_forever()
except KeyboardInterrupt:
    server.server_close()
    face.loop_stop()
    client.loop_stop()
    camera.loop_stop()
    camera.close()
