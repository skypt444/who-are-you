import time
from http.server import BaseHTTPRequestHandler


class CamHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path.endswith('.mjpg'):
            self.send_response(200)
            self.send_header('Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
            self.end_headers()
            try:
                for stream in self.camera.get_last_images_continuous():
                    self.wfile.write(bytes("--jpgboundary", "utf8"))
                    self.send_header('Content-type', 'image/jpeg')
                    self.send_header('Content-length', len(stream.getvalue()))
                    self.end_headers()
                    self.wfile.write(stream.getvalue())
            except KeyboardInterrupt:
                pass
            return
        else:
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes("<html><head></head><body><img src='/cam.mjpg'/></body></html>", "utf8"))
            return
