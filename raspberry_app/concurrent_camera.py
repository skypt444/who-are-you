import threading
import io
import time
import queue

import picamera


class ConcurrentCamera:
    def __init__(self):
        self.camera = picamera.PiCamera()
        self.camera.resolution = (320, 240)
        self.current_stream = None
        self.queue = None

        self._thread = None
        self._thread_terminate = False

    def loop_forever(self):
        stream = io.BytesIO()
        for foo in self.camera.capture_continuous(stream, 'jpeg', use_video_port=True):
            if self._thread_terminate:
                break
            new_stream = io.BytesIO()
            new_stream.write(stream.getvalue())
            self.current_stream = new_stream
            if self.queue is not None:
                self.queue.put(new_stream)
            stream.seek(0)
            stream.truncate()

    def get_last_image(self):
        return self.current_stream

    def get_last_images_continuous(self):
        self.queue = queue.Queue()
        while not self._thread_terminate:
            if not self.queue.empty():
                item = self.queue.get()
                yield item

    def loop_start(self):
        if self._thread is not None:
            return "error"

        self._thread_terminate = False
        self._thread = threading.Thread(target=self.loop_forever)
        self._thread.daemon = True
        self._thread.start()

    def loop_stop(self):
        if self._thread is None:
            return "error"

        self._thread_terminate = True
        if threading.current_thread() != self._thread:
            self._thread.join()
            self._thread = None

    def close(self):
        self.camera.close()
