import threading
import time

import face_recognition
import numpy as np
from PIL import Image


class Face:
    def __init__(self, camera, callback):
        self.camera = camera
        self.callback = callback

        self._thread = None
        self._thread_terminate = False

        self.known_face_encodings = [
            get_encoding_for('resources/ll.jpg'),
            get_encoding_for('resources/tt.jpg')
        ]
        self.known_face_names = [
            "Lee Jihoon",
            "Grevet Theo",
        ]

    def loop_start(self):
        if self._thread is not None:
            return "error"

        self._thread_terminate = False
        self._thread = threading.Thread(target=self.loop_forever)
        self._thread.daemon = True
        self._thread.start()

    def loop_stop(self):
        if self._thread is None:
            return "error"

        self._thread_terminate = True
        if threading.current_thread() != self._thread:
            self._thread.join()
            self._thread = None

    def loop_forever(self):
        while not self._thread_terminate:
            print("capturing an image...")
            img_data = self.camera.get_last_image()
            if img_data is None:
                continue
            img = Image.open(img_data)
            output = np.array(img)
            # output = np.empty((240, 320, 3), dtype=np.uint8)
            print("analyzing the image...")
            start = time.time()
            face_locations = face_recognition.face_locations(output)
            face_encodings = face_recognition.face_encodings(output, face_locations)
            end = time.time()
            runtime = end - start
            print("analyzing finished in {}".format(runtime))

            for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
                matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)

                face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)
                if matches[best_match_index]:
                    name = self.known_face_names[best_match_index]
                    self.callback(name)
                else:
                    print("detected unkown")
            print()


def get_encoding_for(file_name):
    image = face_recognition.load_image_file(file_name)
    return face_recognition.face_encodings(image)[0]

