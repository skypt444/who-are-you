#include <WiFi.h>
#include <PubSubClient.h>

namespace wifi {
  constexpr char* SSID = "hoons";
  constexpr char* PASSWORD = "12341234";
}

namespace pins {
  constexpr int RELAY = 13;
  constexpr int SENSOR = 34;
}

namespace mqtt {
  constexpr char* SERVER = "192.168.43.197";
  constexpr int PORT = 1883;
  namespace topic {
    constexpr char* RELAY = "DL_relay";
    constexpr char* SENSOR = "DL_status";
  }
}

WiFiClient wifi_client;
PubSubClient mqtt_client(wifi_client);

char msg[50];
int value = 0;
bool doorbell_triggered = false;

void setup_wifi() {
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi::SSID);

  WiFi.begin(wifi::SSID, wifi::PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(wifi_client.localIP());
}

void callback_receive(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (strcmp(topic, mqtt::topic::RELAY) == 0) {
    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == '1') {
      digitalWrite(pins::RELAY, HIGH);
      Serial.println("Switch on the relay");
    } else {
      digitalWrite(pins::RELAY, LOW);
      Serial.println("Switch off the relay");
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!mqtt_client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (mqtt_client.connect(clientId.c_str())) {
      Serial.println("connected");
      mqtt_client.subscribe(mqtt::topic::RELAY);
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqtt_client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void check_sensor() {
  int button_state = digitalRead(pins::SENSOR);
  if (button_state == HIGH && !doorbell_triggered)
  {
    Serial.println("sensor triggered, door open");
    mqtt_client.publish(mqtt::topic::SENSOR, "1");
    doorbell_triggered = true;
  }
  if (button_state == LOW && doorbell_triggered)
  { 
    Serial.println("sensor triggered, door close");
    mqtt_client.publish(mqtt::topic::SENSOR, "0");
    doorbell_triggered = false;
  }
}

void setup() {
  pinMode(pins::RELAY, OUTPUT);
  pinMode(pins::SENSOR, INPUT);
  Serial.begin(115200);

  delay(10);
  setup_wifi();

  mqtt_client.setServer(mqtt::SERVER, mqtt::PORT);
  mqtt_client.setCallback(callback_receive);
}

void loop() {
  if (!mqtt_client.connected()) {
    reconnect();
  }

  check_sensor();

  mqtt_client.loop();
}
