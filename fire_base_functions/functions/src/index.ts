import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as crypto from 'crypto';

admin.initializeApp();

export const raspIsOnline = functions.https.onRequest(async (req, res) => {
  const { address, rasp_id } = req.body;
  console.log(`set rasp is online for rasp: ${rasp_id}, address: ${address}`);

  const ref = admin.database().ref().child('/rasp_address/' + rasp_id);
  const value = await ref.once('value');

  if (value.exists()) {
    await ref.set({
      ...value.val(),
      address: address,
    })
  }
  else {
    await ref.set({
      address: address,
    });
  }

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({result: 'ok'}));
};

export const setDoorStatus = functions.https.onRequest(async (req, res) => {
  const { status, rasp_id } = req.body;
  console.log(`set door status for rasp: ${rasp_id}, status: ${status}`);

  const ref = admin.database().ref().child('/door_status/' + rasp_id);
  const value = await ref.once('value');

  if (value.exists()) {
    await ref.set({
      ...value.val(),
      status: status,
    })
  }
  else {
    await ref.set({
      status: status,
    });
  }

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({result: 'ok'}));
});

export const triggerDoorbell = functions.https.onRequest(async (req, res) => {
  const { rasp_id, image_as_b64 } = req.body;
  console.log(`trigger doorbell for rasp: ${rasp_id}`);

  const bucket = admin.storage().bucket();
  const imageBuffer = Buffer.from(image_as_b64, 'base64');
  const imageByteArray = new Uint8Array(imageBuffer);
  const file = bucket.file(`doorbell_images/${crypto.randomBytes(32).toString('hex')}.jpg`);

  await file.save(imageByteArray);

  const urls = await file.getSignedUrl({
    action: 'read',
    expires: '01-01-2500',// not anytime soon
  });

  const url = urls[0];
  console.log(`${file.name} uploaded to ${bucket.name}, url ${url}`);

  await admin.database().ref().child('/doorbell/' + rasp_id).push({
    time: Date.now(),
    image_url: url,
  });

  const payload = {
    notification: {
      title: 'WhoAreYou ! Doorbell',
      body: `Your doorbell has been rang`,
      sound: "default"
    }
  };

  const options = {
    priority: "high",
    timeToLive: 60 * 60 * 24 //24 hours
  };

  await admin.messaging().sendToTopic("Doorbell_Notifications", payload, options);

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({result: 'ok'}));
});

